﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace StreamTokeniser
{
    /// <summary>
    /// A token scanning class to emulate the Java Scanner class.
    /// Base code taken from https://stackoverflow.com/a/26669930
    /// </summary>
    public class Tokeniser
    {
        private static readonly CultureInfo CultureInfo = System.Globalization.CultureInfo.CurrentCulture;
        private TextReader _reader;
        private IConvertible _currentToken;
        private DelimiterMatcher _matcher = new DelimiterMatcher();


        /// <summary>
        /// Sting based stream delimiter
        /// </summary>
        public string Delimiter { get => _matcher.Delimiter; set => _matcher.Delimiter = value; }

        /// <summary>
        /// Regex based stream delimiter
        /// </summary>
        public Regex DelimiterRegex { get => _matcher.DelimiterRegex; set => _matcher.DelimiterRegex = value; }


        /// <summary>
        /// Constructs a stream tokeniser using a stream as the basis for reading tokens
        /// </summary>
        /// <param name="s">A string to read token from</param>
        public Tokeniser(string s) : this(new StringReader(s)) { }

        /// <summary>
        /// Constructs a stream tokeniser using a File as the basis for reading tokens
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public Tokeniser(FileInfo file) : this(file.OpenText()) { }
        
        /// <summary>
        /// Constructs a stream tokeniser using a generic readable text stream as the basis for reading tokens
        /// </summary>
        /// <param name="reader"></param>
        public Tokeniser(TextReader reader) { _reader = reader; }


        /// <summary>
        /// The the Tokeniser has reached the end of the input stream
        /// </summary>
        /// <returns>True if the stream has reached the end, false otherwise</returns>
        public bool EndOfStream { get => _reader.Peek() < 0; }

        
        /// <summary>
        /// Gets the next token in th stream and returns it as a string
        /// </summary>
        /// <returns>Returns the next delimited token as a string</returns>
        public string NextToken() { return ReadNext<string>(); }
        
        /// <summary>
        /// Checks whether the stream has a next token of any type
        /// </summary>
        /// <returns>True if the tream has a next token, false otherwise</returns>
        public bool HasNextToken() { return HasNext<string>(); }

        /// <summary>
        /// Clears the bufferred token to skip over it and return the Tokeniser to an initialised state for reading more tokens.
        /// <para>Useful for when reading a stream of numbers and a non-numeric token is detected in the stream</para>
        /// </summary>
        public void ClearBuffer() { _currentToken = null; }

        /// <summary>
        /// Reads a while line from the stream. Line endings are determined by default line endings and are not configurable
        /// </summary>
        /// <returns>The next line in the stream</returns>
        public string ReadLine() { return _reader.ReadLine(); }
        
        /// <summary>
        /// Reads the next character in the stream
        /// </summary>
        /// <returns>The next character in the stream</returns>
        public char ReadCharacter() { return (char)_reader.Read(); }


        /// <summary>
        /// Peeks at the next token in the stream without reading it off the stream
        /// </summary>
        /// <returns>The next token in stream as a string</returns>
        public string Peek()
        {
            // HasNext will either use the existing _currentToken, or get the next token
            // from the stream and set _currentToken
            return (HasNext<string>())
                ? _currentToken.ToString()
                : "";
        }


        /// <summary>
        /// Reads all tokens in the stream
        /// </summary>
        /// <returns>An iterable enumeration of tokens</returns>
        public IEnumerable<string> ReadAllTokens()
        {
            while (HasNextToken())
                yield return NextToken();
        }

        /// <summary>
        /// Reads all tokens that match the specified type until either the end of stream is reached, or a non-valid token is found.
        /// <para>Note that there may be additional tokens in the stream and users may be required to skip them to resume reading the same type</para>
        /// </summary>
        /// <typeparam name="T">The type of the token to collect and return</typeparam>
        /// <returns>An enumeration of token matching the specified type</returns>
        public IEnumerable<T> ReadUntilBreak<T>() where T : IConvertible
        {
            while (HasNext<T>())
                yield return ReadNext<T>();
        }

        /// <summary>
        /// Reads the next token in the stram and returns it as a specified type.
        /// Upon FormatException, the tokeniser will keep the token bufferred for additional reading.
        /// Users may need to clear the buffer to continue reading from the stream
        /// </summary>
        /// <typeparam name="T">The type to return</typeparam>
        /// <exception cref="System.FormatException">The token cannot be cast to the specified type</exception>
        /// <returns>The next token in the stream</returns>
        public T ReadNext<T>() where T : IConvertible
        {
            if (_currentToken != null)
                return YieldCurrentToken<T>();

            Token token = ReadUntilNextToken();

            return ConvertValue<T>(token.TokenString);
        }

        private T YieldCurrentToken<T>() where T : IConvertible
        {
            // If converting the token throws an error, we won't clear the _currentToken
            var val = ConvertValue<T>(_currentToken);
            _currentToken = null;
            return val;
        }

        private Token ReadUntilNextToken()
        {
            Token token = null;
            EventHandler<Token> lambda = (sender, args) => {
                token = args;
                _matcher.ClearBuffer();
            };

            _matcher.CompletedToken += lambda;

            while(token == null && !EndOfStream) {
                int current = _reader.Read();
                if (current > -1)
                    _matcher.AppendString((char)current);
            }

            if(EndOfStream)
                token = _matcher.YieldTokenFromBuffer(clearBuffer: true);

            _matcher.CompletedToken -= lambda;

            return token;
        }

        private T ConvertValue<T>(IConvertible value) where T : IConvertible
        {
            if (IsEmptyToken(value))
                throw new ArgumentException("Token is empty and cannot be converted to the target type");

            Type type = typeof(T);

            return (type.IsEnum)
                ? (T)Enum.Parse(type, value.ToString())
                : (T)(value).ToType(typeof(T), CultureInfo);
        }

        private bool TryConvertValue<T>(IConvertible value, out T val) where T : IConvertible
        {
            // Try and convert token
            try { val = ConvertValue<T>(value); return true; }
            catch (Exception) { val = default(T); return false; }
        }

        private bool IsEmptyToken(IConvertible value)
        {
            return (value.ToString() == "");
        }

        /// <summary>
        /// Checks whether the stream has a next token of the specified type.
        /// The stream will only read the next token once, and the token stored in a buffer.
        /// Until the token is read out of the buffer, additional calls to this method will return the same result.
        /// </summary>
        /// <typeparam name="T">The type to enforce returned tokens</typeparam>
        /// <returns>True if the next token in the stream matches the specified type, false otherwise</returns>
        public bool HasNext<T>() where T : IConvertible
        {
            T convertedValue;
            if(_currentToken != null)
                return TryConvertValue<T>(_currentToken, out convertedValue);
            
            // Check for end of stream
            if (EndOfStream) return false;

            // Read the next token into the "buffer" or use the existing token if the buffer is not empty
            // We read the token as a forced string to prevent invalid cast exceptions, since we want to check IF if has a next of the given type.
            // This can happen when HasNext<Int> is called with a string in the buffer.
            if (_currentToken == null)
                _currentToken = ReadNext<string>();

            // Check for empty token
            // Would be indicative of empty stream
            if (IsEmptyToken(_currentToken))
            {
                ClearBuffer();
                return false;
            }

            // Now assert the type check
            return TryConvertValue<T>(_currentToken, out convertedValue);
        }

    }
}
