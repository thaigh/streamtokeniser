using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace StreamTokeniser {

    /// <summary>
    /// Returns the token string collected by the delimiter matcher
    /// </summary>
    public class Token {
        /// <summary>
        /// The completed token on the left hand side of the delimiter
        /// </summary>
        public readonly string TokenString;

        /// <summary>
        /// The delimiting string
        /// </summary>
        public readonly string Delimiter;

        public Token(string token, string delimiter) {
            TokenString = token;
            Delimiter = delimiter;
        }

        public override string ToString()
        {
            return string.Format("{0}[{1}]", TokenString, Delimiter);
        }
    }


    /// <summary>
    /// Buffer used to match regex patters and extract tokens when reading from a text readable stream
    /// </summary>
    public class DelimiterMatcher {

        private const string WhitespaceDelimiter = "\\s";

        private Regex _delimiterRegex = new Regex(WhitespaceDelimiter);
        private StringBuilder _buffer = new StringBuilder();

        public event EventHandler<Token> CompletedToken;

        /// <summary>
        /// String representation of regex delimiter pattern
        /// </summary>
        public string Delimiter {
            get => _delimiterRegex.ToString();
            set => SetDelimiter(value);
        }

        /// <summary>
        /// Regex pattern for the Delimiter
        /// </summary>
        public Regex DelimiterRegex { get => _delimiterRegex; set => SetDelimiter(value); }


        /// <summary>
        /// Automatically clear the buffer on matching a delimiter
        /// </summary>
        public bool AutoClear { get; set; } = true;

        /// <summary>
        /// Gets the buffered stream contents
        /// </summary>
        public string BufferContents => _buffer.ToString();

        /// <summary>
        /// Creates a delimiter matcher using whitespace as the default delimiter pattern
        /// </summary>
        public DelimiterMatcher() { ResetDelimiter(); }

        /// <summary>
        /// Creates a delimiter matcher using a string as the delimiter pattern to match against
        /// </summary>
        /// <param name="delimiter"></param>
        public DelimiterMatcher(string delimiter) { SetDelimiter(delimiter); }

        /// <summary>
        /// Creates a delimiter matcher using a Regex pattern to match against
        /// </summary>
        /// <param name="pattern"></param>
        public DelimiterMatcher(Regex pattern) { SetDelimiter(pattern); }


        /// <summary>
        /// Sets the delimiter to match against
        /// </summary>
        /// <param name="delimiter">A non empty string pattern</param>
        public void SetDelimiter(string delimiter) {
            if (string.IsNullOrEmpty(delimiter))
                throw new ArgumentException("Delimiter cannot be null");

            string escaped = Regex.Escape(delimiter);
            _delimiterRegex = new Regex(escaped);
        }

        /// <summary>
        /// Sets the delimiter to match against
        /// </summary>
        /// <param name="pattern">A regex pattern</param>
        public void SetDelimiter(Regex pattern) {
            if(pattern == null)
                throw new ArgumentNullException(nameof(pattern));

            _delimiterRegex = pattern;
        }


        /// <summary>
        /// Resets the delimiter pattern to use whitespace pattern matching
        /// </summary>
        public void ResetDelimiter() { SetDelimiter(new Regex(WhitespaceDelimiter)); }


        /// <summary>
        /// Appends a string to the buffer and attempts to match against the set delimiter pattern
        /// </summary>
        /// <param name="s">The string to append to the buffer</param>
        public void AppendString(string s) {
            _buffer.Append(s);
            if (BufferContainsDelimiter())
                RaiseCompletedTokenEvent();
        }


        /// <summary>
        /// Appends a single character to the buffer and attempts to match against the set delimiter pattern
        /// </summary>
        /// <param name="c">The char to append to the buffer</param>
        public void AppendString(char c) {
            _buffer.Append(c);
            if (BufferContainsDelimiter())
                RaiseCompletedTokenEvent();
        }


        private bool BufferContainsDelimiter() { return _delimiterRegex.IsMatch(BufferContents); }

        private Token ExtractTokenFromBuffer() {
            // Remove Delimiter from bufferred string
            // Can't use substring() since regex string will be something like "\s"
            // But the delimiting string will be a single character " "
            string[] tokens = _delimiterRegex.Split(BufferContents);

            // It is possible for tokens to be empty
            if (tokens.Length == 0) return null;
            if (tokens[0].Length == 0) return null;
            
            string tokenString = tokens[0];
            string delimiterString = BufferContents.Substring(tokenString.Length);
            return new Token(tokenString, delimiterString);
        }

        private void RaiseCompletedTokenEvent() {
            
            Token token = ExtractTokenFromBuffer();

            // Only raise is there is content in the buffer
            // E.g. Don't raise multiple events where we are consuming whitespace without reading a non-whitespace character
            if(token != null) {
                CompletedToken?.Invoke(this, token);

                if (AutoClear)
                    ClearBuffer();

            } else {
                ClearBuffer();
            }
        }

        /// <summary>
        /// Clears the buffer of its contents
        /// </summary>
        public void ClearBuffer() { _buffer.Clear(); }

        /// <summary>
        /// Returns the contents of the buffer as a token. Optionally clears the buffer on yield.
        /// </summary>
        /// <returns></returns>
        public Token YieldTokenFromBuffer(bool clearBuffer) {
            Token t = new Token(_buffer.ToString(), "");
            
            if(clearBuffer)
                ClearBuffer();

            return t;
        }

    }
}