using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Xunit;

namespace StreamTokeniser.Tests {

    public class DelimiterMatcherTests {

        private List<Token> _returnedTokens = new List<Token>();
        private DelimiterMatcher _matcher = new DelimiterMatcher();

        public DelimiterMatcherTests() {
            _returnedTokens = new List<Token>();
            _matcher = new DelimiterMatcher();
        }

        private void HandleCompletedTokenEvent(object sender, Token args) {
            _returnedTokens.Add(args);
            _matcher.ClearBuffer();
        }

        private void FeedMatcher(DelimiterMatcher matcher, string content) {
            int i = 0;
            while(i < content.Length) {
                matcher.AppendString(content[i++]);
            }
        }


        [Fact]
        public void DelimiterMatcher_DelimiterString_ConstructorDefault() {
            DelimiterMatcher matcher = new DelimiterMatcher();
            Assert.Equal("\\s", matcher.Delimiter);
        }

        [Fact]
        public void DelimiterMatcher_DelimiterString_ConstructorInitialisedString() {
            DelimiterMatcher matcher = new DelimiterMatcher("\t");
            Assert.Equal("\\t", matcher.Delimiter);
        }

        [Fact]
        public void DelimiterMatcher_DelimiterString_ConstructorInitialisedRegex() {
            DelimiterMatcher matcher = new DelimiterMatcher(new Regex("[aA]"));
            Assert.Equal("[aA]", matcher.Delimiter);
        }


        [Fact]
        public void DelimiterMatcher_SetDelimiter_String() {
            _matcher = new DelimiterMatcher();
            Assert.Equal("\\s", _matcher.Delimiter);

            _matcher.SetDelimiter("\t");
            Assert.Equal("\\t", _matcher.Delimiter);

            _matcher.Delimiter = "[aA]";
            Assert.Equal("\\[aA]", _matcher.Delimiter);
        }

        [Fact]
        public void DelimiterMatcher_SetDelimiter_NullOrEmptyString() {
            _matcher = new DelimiterMatcher();
            Assert.Equal("\\s", _matcher.Delimiter);

            Assert.Throws<ArgumentException>(() => _matcher.SetDelimiter((string)null));
            Assert.Throws<ArgumentException>(() => _matcher.SetDelimiter((string)""));
        }

        [Fact]
        public void DelimiterMatcher_SetDelimiter_Regex() {
            _matcher = new DelimiterMatcher();
            Assert.Equal("\\s", _matcher.Delimiter);

            _matcher.SetDelimiter(new Regex("\t"));
            Assert.Equal("\t", _matcher.Delimiter);
        }

        
        [Fact]
        public void DelimiterMatcher_MatchWhiteSpace_Space() {
            _matcher = new DelimiterMatcher();
            _matcher.CompletedToken += HandleCompletedTokenEvent;

            string content = "hello ";
            string expected = "hello";

            FeedMatcher(_matcher, content);

            Assert.Equal(expected, _returnedTokens[0].TokenString);
            Assert.Equal(" ", _returnedTokens[0].Delimiter);

            _matcher.CompletedToken -= HandleCompletedTokenEvent;
        }

        [Fact]
        public void DelimiterMatcher_MatchWhiteSpace_MultipleSpaces() {
            _matcher = new DelimiterMatcher();
            _matcher.CompletedToken += HandleCompletedTokenEvent;

            string content = "  hello\t\tworld\n\n!";

            FeedMatcher(_matcher, content);

            Assert.Equal("hello", _returnedTokens[0].TokenString);
            Assert.Equal("\t", _returnedTokens[0].Delimiter);
            Assert.Equal("world", _returnedTokens[1].TokenString);
            Assert.Equal("\n", _returnedTokens[1].Delimiter);

            _matcher.CompletedToken -= HandleCompletedTokenEvent;
        }

        [Fact]
        public void DelimiterMatcher_MatchChar() {
            _matcher = new DelimiterMatcher("|");
            _matcher.CompletedToken += HandleCompletedTokenEvent;

            string content = "hello|world|";

            FeedMatcher(_matcher, content);

            Assert.Equal("hello", _returnedTokens[0].TokenString);
            Assert.Equal("|", _returnedTokens[0].Delimiter);
            Assert.Equal("world", _returnedTokens[1].TokenString);
            Assert.Equal("|", _returnedTokens[1].Delimiter);

            _matcher.CompletedToken -= HandleCompletedTokenEvent;
        }

        [Fact]
        public void DelimiterMatcher_MatchMultiChars() {
            _matcher = new DelimiterMatcher("zzz");
            _matcher.CompletedToken += HandleCompletedTokenEvent;

            string content = "hello zzz world zzz";

            FeedMatcher(_matcher, content);

            Assert.Equal("hello ", _returnedTokens[0].TokenString);
            Assert.Equal("zzz", _returnedTokens[0].Delimiter);
            Assert.Equal(" world ", _returnedTokens[1].TokenString);
            Assert.Equal("zzz", _returnedTokens[1].Delimiter);

            _matcher.CompletedToken -= HandleCompletedTokenEvent;
        }

        [Fact]
        public void DelimiterMatcher_MatchRegex() {
            _matcher = new DelimiterMatcher(new Regex("[aA]"));
            _matcher.CompletedToken += HandleCompletedTokenEvent;

            string content = "hello a world A";

            FeedMatcher(_matcher, content);

            Assert.Equal("hello ", _returnedTokens[0].TokenString);
            Assert.Equal("a", _returnedTokens[0].Delimiter);
            Assert.Equal(" world ", _returnedTokens[1].TokenString);
            Assert.Equal("A", _returnedTokens[1].Delimiter);

            _matcher.CompletedToken -= HandleCompletedTokenEvent;
        }

    }
}