using System;
using Xunit;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace StreamTokeniser.Tests
{
    public class TokeniserTests
    {
        #region SingleWord
        [Fact]
        public void Tokeniser_SingleWord_ReadNextString()
        {
            string stream = " hello";
            string expected = "hello";
            Tokeniser st = new Tokeniser(stream);

            Assert.Equal(true, st.HasNext<string>());
            string token = st.ReadNext<string>();
            Assert.Equal(expected, token);
            Assert.Equal(false, st.HasNext<string>());
            Assert.Equal(true, st.EndOfStream);
        }

        [Fact]
        public void Tokeniser_SingleWord_NextToken()
        {
            string stream = " hello";
            string expected = "hello";
            Tokeniser st = new Tokeniser(stream);

            Assert.Equal(true, st.HasNextToken());
            string token = st.NextToken();
            Assert.Equal(expected, token);
            Assert.Equal(false, st.HasNextToken());
            Assert.Equal(true, st.EndOfStream);
        }

        [Fact]
        public void Tokeniser_SingleWord_AllTokens_OneToken()
        {
            string stream = " hello";
            string expected = "hello";
            Tokeniser st = new Tokeniser(stream);

            string[] tokens = st.ReadAllTokens().ToArray();
            Assert.Equal(1, tokens.Length);
            Assert.Equal(expected, tokens[0]);
            Assert.Equal(false, st.HasNextToken());
            Assert.Equal(true, st.EndOfStream);
        }

        [Fact]
        public void Tokeniser_SingleWord_ReadLine()
        {
            string stream = " hello";
            string expected = " hello";
            Tokeniser st = new Tokeniser(stream);

            string token = st.ReadLine();
            Assert.Equal(expected, token);
        }

        [Fact]
        public void Tokeniser_MultiWord_SingleLine_ReadLine()
        {
            string stream = " hello world ";
            string expected = " hello world ";
            Tokeniser st = new Tokeniser(stream);

            string token = st.ReadLine();
            Assert.Equal(expected, token);
        }

        [Fact]
        public void Tokeniser_MultiWord_MultiLine_ReadLine()
        {
            string stream = " hello\nworld ";
            string expected = " hello";
            Tokeniser st = new Tokeniser(stream);

            string token = st.ReadLine();
            Assert.Equal(expected, token);
        }

        [Fact]
        public void Tokeniser_SingleWord_ReadCharacter_LeadingSpace()
        {
            string stream = " hello";
            Tokeniser st = new Tokeniser(stream);

            char token = st.ReadCharacter();
            Assert.Equal(' ', token);
        }

        [Fact]
        public void Tokeniser_SingleWord_ReadCharacter()
        {
            string stream = "hello";
            Tokeniser st = new Tokeniser(stream);

            char token = st.ReadCharacter();
            Assert.Equal('h', token);
        }

        #endregion

        #region MultipleWord

        [Fact]
        public void Tokeniser_AllTokens_MultipleTokens()
        {
            string stream = " hello\t12\nFalse 1.123";
            Tokeniser st = new Tokeniser(stream);

            string[] tokens = st.ReadAllTokens().ToArray();
            
            Assert.Equal(4, tokens.Length);
            Assert.Equal("hello", tokens[0]);
            Assert.Equal("12", tokens[1]);
            Assert.Equal("False", tokens[2]);
            Assert.Equal("1.123", tokens[3]);

            Assert.Equal(false, st.HasNextToken());
            Assert.Equal(true, st.EndOfStream);
        }

        [Fact]
        public void Tokeniser_MultipleWordsWhitespace()
        {
            string stream = "hello world!  This   is a delimited            string!";
            Tokeniser st = new Tokeniser(stream);

            string[] tokens = stream.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            string[] allTokens = st.ReadAllTokens().ToArray();

            Assert.Equal(tokens.Length, allTokens.Length);
            for (int i = 0; i < tokens.Length; i++)
            {
                Assert.Equal(tokens[i], allTokens[i]);
            }
        }

        #endregion

        #region InvalidCasts

        [Fact]
        public void Tokeniser_InvalidInt_ThrowError()
        {
            string stream = "12.1";
            Tokeniser st = new Tokeniser(stream);

            Assert.Throws<FormatException>(() => st.ReadNext<int>());
        }

        [Fact]
        public void Tokeniser_InvalidDouble_ThrowError()
        {
            string stream = "error";
            Tokeniser st = new Tokeniser(stream);

            Assert.Throws<FormatException>(() => st.ReadNext<double>());
        }

        [Fact]
        public void Tokeniser_InvalidInt_NonNumeric_ThrowError()
        {
            string stream = "error";
            Tokeniser st = new Tokeniser(stream);

            Assert.Throws<FormatException>(() => st.ReadNext<int>());
        }

        [Fact]
        public void Tokeniser_InvalidBool_ThrowError()
        {
            string stream = "NonBool";
            Tokeniser st = new Tokeniser(stream);

            Assert.Throws<FormatException>(() => st.ReadNext<bool>());
        }

        #endregion

        #region ValidCasts

        [Fact]
        public void Tokeniser_ValidInt()
        {
            string stream = "12";
            Tokeniser st = new Tokeniser(stream);

            Assert.Equal(12, st.ReadNext<int>());
        }

        [Fact]
        public void Tokeniser_ValidDouble()
        {
            string stream = "12.1";
            Tokeniser st = new Tokeniser(stream);

            Assert.Equal(12.1, st.ReadNext<double>());
        }

        [Fact]
        public void Tokeniser_ValidBool_TextTrue()
        {
            string stream = "True";
            Tokeniser st = new Tokeniser(stream);
            Assert.Equal(true, st.ReadNext<bool>());
        }

        [Fact]
        public void Tokeniser_ValidBool_TextFalse()
        {
            string stream = "False";
            Tokeniser st = new Tokeniser(stream);

            Assert.Equal(false, st.ReadNext<bool>());
        }

        #endregion

        #region HasNext
        [Fact]
        public void Tokeniser_HasNext_Repeatedly()
        {
            string stream = "12";
            Tokeniser st = new Tokeniser(stream);

            Assert.Equal(true, st.HasNext<int>());
            Assert.Equal(true, st.HasNext<int>());
            Assert.Equal(true, st.HasNext<int>());

            Assert.Equal(12, st.ReadNext<int>());

            Assert.Equal(false, st.HasNext<int>());
            Assert.Equal(false, st.HasNext<int>());
            Assert.Equal(false, st.HasNext<int>());
        }

        [Fact]
        public void Tokeniser_HasNext_YesNo()
        {
            string stream = "12 String 12";
            Tokeniser st = new Tokeniser(stream);

            Assert.Equal(true, st.HasNext<int>());
            Assert.Equal(true, st.HasNext<int>());
            Assert.Equal(true, st.HasNext<int>());

            Assert.Equal(12, st.ReadNext<int>());

            Assert.Equal(false, st.HasNext<int>());
            Assert.Equal(false, st.HasNext<int>());
            Assert.Equal(false, st.HasNext<int>());

            Assert.Equal("String", st.ReadNext<string>());

            Assert.Equal(true, st.HasNext<int>());
            Assert.Equal(true, st.HasNext<int>());
            Assert.Equal(true, st.HasNext<int>());

            Assert.Equal(12, st.ReadNext<int>());

            Assert.Equal(false, st.HasNext<int>());
            Assert.Equal(false, st.HasNext<int>());
            Assert.Equal(false, st.HasNext<int>());
        }

        #endregion

        [Fact]
        public void Readme_Reading() {
            string stream = "12 String False 1.23";
            Tokeniser tok = new Tokeniser(stream);

            int number = tok.ReadNext<int>();
            string str = tok.ReadNext<string>();
            bool mBool = tok.ReadNext<bool>();
            double num = tok.ReadNext<double>();

            Assert.Equal(12, number);
            Assert.Equal("String", str);
            Assert.Equal(false, mBool);
            Assert.Equal(1.23, num);

            // Reset Stream
            tok = new Tokeniser(stream);
            while(tok.HasNext<int>()) {
                int i = tok.ReadNext<int>();
                Assert.Equal(12, i);
            }
        }


        [Fact]
        public void Readme_Regex() {
            string stream = " 12 | String , False > 1.23 ";
            Tokeniser tok = new Tokeniser(stream);
            tok.DelimiterRegex = new Regex("[|,>]");

            Assert.Equal(" 12 ", tok.ReadNext<string>());
            Assert.Equal(" String ", tok.ReadNext<string>());
            Assert.Equal(" False ", tok.ReadNext<string>());
            Assert.Equal(" 1.23 ", tok.ReadNext<string>());
        }


        [Fact]
        public void Readme_StringDelimiter() {
            string stream = " 12 ZZ String ZZ False ZZ 1.23 ";
            Tokeniser tok = new Tokeniser(stream);
            tok.Delimiter = "ZZ";

            Assert.Equal(" 12 ", tok.ReadNext<string>());
            Assert.Equal(" String ", tok.ReadNext<string>());
            Assert.Equal(" False ", tok.ReadNext<string>());
            Assert.Equal(" 1.23 ", tok.ReadNext<string>());
        }

    }
}
