PROJ=StreamTokeniser
TEST=StreamTokeniser.Tests
NUGET_OUT=NugetBuilds
MYGET_REPO=https://www.myget.org/F/tylerhaigh/api/v2/package
BUILD_CONFIG=Release

clean:
	dotnet clean $(PROJ)
	dotnet clean $(TEST)
	rm -rf $(NUGET_OUT)
	rm -rf $(COVERAGE_REPORT_ROOT)

build:
	dotnet restore
	dotnet build $(PROJ) -c $(BUILD_CONFIG)
	dotnet build $(TEST) -c $(BUILD_CONFIG)

test: clean build
	dotnet test $(TEST)

pack: 
	dotnet pack $(PROJ) -c $(BUILD_CONFIG) --no-build --output ../$(NUGET_OUT)

publish:
ifndef API_KEY
	$(error API_KEY not set from Command Line. Cannot publish Nuget Package)
else
	nuget push $(NUGET_OUT)/*.nupkg $(API_KEY) -Source $(MYGET_REPO)
endif

all: clean build test pack publish


# Coverage Test Support

OPEN_COVER=D:/App/OpenCover/OpenCover.Console.exe
DOTNET="C:\Program Files\dotnet\dotnet.exe"
REPORT_GENERATOR=D:/App/ReportGenerator/ReportGenerator.exe
BUILD_FRAMEWORK=netcoreapp2.0

COVERAGE_REPORT_ROOT=CoverageReports
COVERAGE_REPORT_INPUT=$(COVERAGE_REPORT_ROOT)/AC15.Tests.coverage.xml
COVERAGE_REPORT_OUTPUT=$(COVERAGE_REPORT_ROOT)/AC15.Tests.coverage.report

test-coverage: clean build
	mkdir -p $(COVERAGE_REPORT_ROOT)
	$(OPEN_COVER) \
	-register:user \
	-target:$(DOTNET) \
	-targetargs:"test -f $(BUILD_FRAMEWORK) -c $(BUILD_CONFIG) $(TEST)" \
	-output:$(COVERAGE_REPORT_INPUT) \
	-oldStyle

test-coverage-report:
	rm -rf $(COVERAGE_REPORT_OUTPUT)
	$(REPORT_GENERATOR) \
	-reports:$(COVERAGE_REPORT_INPUT) \
	-targetdir:$(COVERAGE_REPORT_OUTPUT) \

test-coverage-all: test-coverage test-coverage-report	