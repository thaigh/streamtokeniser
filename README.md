# Stream Tokeniser #

A token-based stream reading library.

## Why ##

The [Java Scanner](https://docs.oracle.com/javase/7/docs/api/java/util/Scanner.html) class provides a nice interface for reading strongly typed tokens from a stream instance. These tokens can be read as if it were a stream of numbers, strings, etc. However, I couldn't find existing equivalent functionality for dotnet.

The [Stack Overflow](https://stackoverflow.com/questions/722270/is-there-an-equivalent-to-the-scanner-class-in-c-sharp-for-strings/26669930#26669930) community have come up with some base code for reading from a stream using whitespace delimiters, but this library goes one step further allowing users to define their own string or regex-based delimiter

## Usage ##

You can start reading from a stream using one of three possible options:

* Strings
* Files (via [FileInfo](https://docs.microsoft.com/en-us/dotnet/api/system.io.fileinfo?view=netframework-4.7.2))
* Any readable stream inheriting from [TextReader](https://docs.microsoft.com/en-us/dotnet/api/system.io.textreader?view=netframework-4.7.2)

The Tokeniser interface exposes a generic interface for reading any primitive type (int, string, bool, double)

```
string stream = "12 String False 1.23";
Tokeniser tok = new Tokeniser(stream);

int number = tok.Next<int>();       // 12
string str = tok.Next<string>();    // "String"
bool mBool = tok.Next<bool>();      // false
double num = tok.Next<double>();    // 1.23
```

The tokeniser also allows for checking the type of the next token in the stream to enable continuous reading

```
while(tok.HasNext<int>()) {
    int i = tok.ReadNext<int>();
    Console.WriteLine(i);
}

// Prints 12 since the next token is a string
```

You can specify your own delimiters either with direct strings, or using [Regex](https://docs.microsoft.com/en-us/dotnet/api/system.text.regularexpressions.regex?view=netframework-4.7.2) patterns

```
string stream = " 12 ZZ String ZZ False ZZ 1.23 ";
Tokeniser tok = new Tokeniser(stream);
tok.Delimiter = "ZZ";

while(tok.HasNext<string>()) {
    string s = tok.ReadNext<string>();
    Console.WriteLine("\\"" + s + "\\"");
}

// Prints:
// " 12 "
// " String "
// " False "
// " 1.23 "
// Note that whitespaces are included in the collected tokens since they are not part of the delimiting string
```

```
string stream = " 12 | String , False > 1.23 ";
Tokeniser tok = new Tokeniser(stream);
tok.DelimiterRegex = new Regex("[|,>]");

while(tok.HasNext<string>()) {
    string s = tok.ReadNext<string>();
    Console.WriteLine("\\"" + s + "\\"");
}

// Prints:
// " 12 "
// " String "
// " False "
// " 1.23 "
// Note that whitespaces are included in the collected tokens since they are not part of the regex delimiting expression
```